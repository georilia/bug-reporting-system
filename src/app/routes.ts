import { Routes } from '@angular/router';
import { TableComponent } from './table/table/table.component';
import { CreateBugComponent } from './create-new-bug/create-bug/create-bug.component';


export const routes: Routes = [
       { path: '', component: TableComponent},
       { path: 'createbug', component: CreateBugComponent },
       { path: 'editbug/:id', component: CreateBugComponent }
 
   ];

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateBugComponent } from './create-bug/create-bug.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { routes } from 'src/app/routes';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [CreateBugComponent],
  imports: [
    CommonModule, ReactiveFormsModule, RouterModule.forRoot(routes), HttpClientModule, BrowserAnimationsModule
  ],
  exports: [CreateBugComponent]
})
export class CreateNewBugModule { }

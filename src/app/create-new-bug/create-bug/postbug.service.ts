import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bugsreport } from 'src/app/table/table/getserverdata.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostbugService {
  url = 'https://bug-report-system-server.herokuapp.com/bugs/';
  constructor(private http: HttpClient) { }



  getBugsId(bugid): Observable<Bugsreport[]> {
    return  this.http.get<Bugsreport[]>(`${this.url}${bugid}`); }

  sentBugs(bugs: Bugsreport[]) {
    this.http.post(`${this.url}`, bugs).subscribe(data => {console.log(data); }); }

  putBugId(bugid, newBug: Bugsreport[]) {
    return this.http.put(`${this.url}${bugid}`, newBug); }

  deleteBugId(bugid) {
   return this.http.delete(`${this.url}${bugid}`); }
  }

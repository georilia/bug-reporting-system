import { TestBed } from '@angular/core/testing';

import { PostbugService } from './postbug.service';

describe('PostbugService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostbugService = TestBed.get(PostbugService);
    expect(service).toBeTruthy();
  });
});

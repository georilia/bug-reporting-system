import { Component, OnInit, OnChanges } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { PostbugService } from './postbug.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Bugsreport } from 'src/app/table/table/getserverdata.service';
import { trigger, state, style, animate, transition } from '@angular/animations';



@Component({
  selector: 'app-create-bug',
  templateUrl: './create-bug.component.html',
  styleUrls: ['./create-bug.component.css'],
  animations: [
    // the fade-in/fade-out animation.
    trigger('simpleFadeAnimation', [

      // the "in" style determines the "resting" state of the element when it is visible.
      state('in', style({ opacity: 1 })),

      // fade in when created. this could also be written as transition('void => *')
      transition(':enter', [
        style({ opacity: 0 }),
        animate(600)
      ]),

      // fade out when destroyed. this could also be written as transition('void => *')
      transition(':leave',
        animate(600, style({ opacity: 0 })))
    ])
  ]

})
export class CreateBugComponent implements OnInit {
  bugVal: Bugsreport[];
  comShow = false;
  divShow = false; // metavliti gia na deixnei-krivei ta koumpia
  createBugForm: FormGroup;
  commentS: FormGroup;
  editbugId: { id: string };
  newComment: any;
  id: string;
  newBug: Bugsreport[];
  bugById: Bugsreport[];
  oldComments: string[];
  previewComments: string[];

  constructor(private postbugService: PostbugService, private routes: ActivatedRoute, private route: Router) { }





  ngOnInit() {
    this.createBugForm = new FormGroup({
      title: new FormControl('', [Validators.required, Validators.maxLength(20), Validators.minLength(3)]),
      description: new FormControl('', [Validators.required, Validators.maxLength(40), Validators.minLength(3)]),
      priority: new FormControl('', [Validators.required]),
      reporter: new FormControl('', [Validators.required]),
      status: new FormControl('', Validators.required),
      comments: new FormArray([])
    });

    this.commentS = new FormGroup({
      reporter: new FormControl('', [Validators.required, Validators.maxLength(12), Validators.minLength(3)]),
      description: new FormControl('', [Validators.required, Validators.maxLength(50), Validators.minLength(3)])
    });


    this.editbugId = { id: this.routes.snapshot.params['id'] };
    this.id = this.editbugId.id;


    if (this.id) {
      this.divShow = true; // it changes to hide-show the buttons in edit - create pages
      this.postbugService.getBugsId(this.id).subscribe(data => {
        this.bugById = data;
        this.getBugg();
      });
    }
  }


  get status() { return this.createBugForm.get('status'); }
  get priority() { return this.createBugForm.get('priority'); }
  get title() { return this.createBugForm.get('title'); }
  get reporter() { return this.createBugForm.get('reporter'); }
  get description() { return this.createBugForm.get('description'); }
  get reporterCom() { return this.commentS.get('reporter'); }
  get descriptionCom() { return this.commentS.get('description'); }

  getBugg(): any {
    this.bugVal = Object.values(this.bugById); // array of values
    this.onFormUpdate(this.bugVal); // dedomena pou pira apo to server palia dedomena
  }

  onFormSubmit() { // posts the new created bug
    this.newBug = this.createBugForm.value; // array of vallues
    this.postbugService.sentBugs(this.newBug);
    console.log(this.newBug);
    this.route.navigate(['']);
    alert('Your bug got registered, press ok to go back to home page');
  }

  onPutFormUpdate(x) { // submits with put the updated bug values

    if (x === 1) {
      this.createBugForm.value.comments = this.createBugForm.value.comments.concat(this.bugVal[8]);
      this.route.navigate(['']);
      alert('Your bug got updated, press ok to go back to home page');
    }
    if (x === 2) {
      this.submitComment();
      alert('Your comment got submited');
    }
    console.log(this.createBugForm.value.comments);
    this.newBug = this.createBugForm.value;
    this.postbugService.putBugId(this.editbugId.id, this.newBug).subscribe(data => { console.log(data); });
  }

  onDeleteBug() { // deletes the selected bug
    if (confirm(`Are you sure to delete the bug with the ID ${this.editbugId.id} ?`)) {
      this.postbugService.deleteBugId(this.editbugId.id).subscribe(result => {
        alert('Your bug got deleted, press ok to go back to home page');
        this.route.navigate(['']);
      });

    }
  }


  onFormUpdate(bg: any) { // loads the values to the form
    this.createBugForm.patchValue({
      title: bg[1],
      description: bg[2],
      priority: JSON.stringify(bg[3]),
      reporter: bg[4],
      status: bg[5]
    });

    if (bg[8]) { // checks if at the 8 of the data the array has data inside
      this.oldComments = Object.values(bg[8]); // if there is data put it in the variable
      this.previewComments = this.oldComments;
      if (this.previewComments.length > 0){ // checks if there are old comments to show the table 
        this.comShow = true; }
    }
  }

  submitComment() {
    if (this.oldComments.length > 0) { // check if there are old comments
      this.oldComments.push(this.commentS.value); // me to klik tou dino the new values inside the old comments
      // take new and old comment values and give it to the ready to sumbit form
      this.createBugForm.value.comments = this.createBugForm.value.comments.concat(this.oldComments);
      this.oldComments = [];
    }
    else { // if there are no new comments just take the new and push it into the array
      this.createBugForm.value.comments = this.createBugForm.value.comments.concat(this.commentS.value);
      this.previewComments = this.createBugForm.value.comments;
      this.comShow = true;
    }
  }




}










import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TableModule } from './table/table/table.module';
import { RouterModule } from '@angular/router';
import { routes } from './routes';
import { CreateNewBugModule } from './create-new-bug/create-new-bug.module';
import { ReactiveFormsModule } from '@angular/forms';
import { authorizedUsersInterceptor } from './customInterceptor';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TableModule,
    HttpClientModule,
    RouterModule.forRoot(routes, {useHash: true}),
    CreateNewBugModule,
    ReactiveFormsModule

  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: authorizedUsersInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }

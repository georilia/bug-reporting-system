import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table.component';
import { RouterModule } from '@angular/router';
import { routes } from 'src/app/routes';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [TableComponent],
  imports: [
    CommonModule, RouterModule.forRoot(routes), BrowserAnimationsModule, ReactiveFormsModule
  ],
  exports: [TableComponent, RouterModule]
})
export class TableModule { }

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetserverdataService {
  url = 'https://bug-report-system-server.herokuapp.com/bugs';
  constructor(private http: HttpClient) { }

getPages() {
  return this.http.get<any>(`${this.url}`, {observe: 'response'});
}

getBugsSorted(searchBug , tlt, ord): Observable<Bugsreport[]> {
    let status = searchBug.value.status;
    let priority = searchBug.value.priority;
    let reporter = searchBug.value.reporter;
    let title = searchBug.value.title;
    return this.http.get<Bugsreport[]>(`${this.url}?sort=${tlt}${ord}&status=${status}&title=${title}&priority=${priority}&reporter=${reporter}`);
  }

getBugsbyPage(pageNum) {
    return this.http.get<Bugsreport[]>(`${this.url}?page=${pageNum}`);
  }
}
export interface Bugsreport {
    title: string;
    priority: string;
    reporter: string;
    createdAt: string;
    status: string;
    id: number;
    comments: {
      id: string,
      reporter: string ,
      description: string };
  }


import { Component, OnInit } from '@angular/core';
import { GetserverdataService, Bugsreport } from './getserverdata.service';
import { Router } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  animations: [
    // the fade-in/fade-out animation.
    trigger('simpleFadeAnimation', [

      // the "in" style determines the "resting" state of the element when it is visible.
      state('in', style({ opacity: 1 })),

      // fade in when created. this could also be written as transition('void => *')
      transition(':enter', [
        style({ opacity: 0 }),
        animate(600)
      ]),

      // fade out when destroyed. this could also be written as transition('void => *')
      transition(':leave',
        animate(600, style({ opacity: 0.5 })))
    ])
  ]

})
export class TableComponent implements OnInit {
  searchBug: FormGroup;
  clicked = true;
  items: Bugsreport[];
  bugid: string;
  pagesNum: string;
  pages: number[];
  currentPage: number = 0;
  lastPage: number;
  searched: Bugsreport[];

  constructor(private getserverdataService: GetserverdataService, private route: Router) { }

  ngOnInit() {
    this.searchBug = new FormGroup({
      title: new FormControl(''),
      status: new FormControl(''),
      priority: new FormControl(''),
      reporter: new FormControl('')
    });



    this.getserverdataService.getBugsSorted(this.searchBug, '', '').subscribe(data => { this.items = data; });
    this.getserverdataService.getPages().subscribe(responce => { this.pagesNum = responce.headers.get('totalPages'); this.pagesGen(); });
  }

  onClickId(bugid: string) {
    this.route.navigate(['/editbug', bugid]);
  }


  pagesGen() {
    this.lastPage = Number(this.pagesNum); // convert the string we get with how many pages with bugs has the server
    this.pages = Array.from(Array(this.lastPage + 1).keys()); // create an array with that many numbers[1,2,3, etc..]
    this.pages.shift(); // remove the zero

  }

  open(pageNum) {
    this.getserverdataService.getBugsbyPage(pageNum).subscribe(data => { this.items = data; });
    this.currentPage = pageNum;


  }

  nextPreviousPage(x) {
    if (this.currentPage > 0 && x < 0) {
      this.currentPage--;
      this.getserverdataService.getBugsbyPage(this.currentPage).subscribe(data => { this.items = data; });
    }

    if (this.currentPage < this.lastPage - 1 && x > 0) {
      this.currentPage++;
      this.getserverdataService.getBugsbyPage(this.currentPage).subscribe(data => { this.items = data; });
    }
  }

  onSearch(tlt, order) {
    this.getserverdataService.getBugsSorted(this.searchBug, tlt, order).subscribe(data => { this.items = data; });
  }

  onShort(tlt: string, order: string) {
    if (this.clicked) { this.getserverdataService.getBugsSorted(this.searchBug, tlt, order).subscribe(data => { this.items = data; }); }

      else { this.getserverdataService.getBugsSorted(this.searchBug, tlt, order = ',asc').subscribe(data => { this.items = data; }); }

    this.clicked = !this.clicked;
  }

}

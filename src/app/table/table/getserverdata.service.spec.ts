import { TestBed } from '@angular/core/testing';

import { GetserverdataService } from './getserverdata.service';

describe('GetserverdataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetserverdataService = TestBed.get(GetserverdataService);
    expect(service).toBeTruthy();
  });
});
